package ntnu.idatt2001.kristina.oblig3.cardgame;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

public class AlertBox {

    public static void display(String title, String message) {
        // STAGE
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setMinHeight(100);

        // MESSAGE LABEL
        Label label = new Label();
        label.setText(message);

        // CLOSE BUTTON
        Button closeButton = new Button("Close");
        closeButton.setOnAction(e -> window.close());

        // LAYOUT
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);

        // SCENE
        Scene scene = new Scene(layout);
        window.setScene(scene);

        window.showAndWait();
    }

}
