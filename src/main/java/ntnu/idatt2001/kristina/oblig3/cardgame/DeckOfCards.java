package ntnu.idatt2001.kristina.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a complete deck of cards consisting of 52 playing cards.
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> playingCards;

    /**
     * The suits spades, hearts, diamons and clubs are represented by the characters
     * S, H, D and C. There are 13 cards in each suit with face from 1 to 13.
     */
    public DeckOfCards() {
        playingCards = new ArrayList<PlayingCard>();
        char[] suits = {'S', 'H', 'D', 'C'};
        for(char suit : suits) {
            for (int face = 1; face < 14; face++) {
                playingCards.add(new PlayingCard(suit,face));
            }
        }

    }

    public ArrayList<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    class HandOfCards {
        private ArrayList<PlayingCard> playingCards;

        /**
         * @param n The number of cards on the hand.
         * @throws IllegalArgumentException If the argument isn't an integer between 5 and 52.
         */
        public HandOfCards(int n) throws IllegalArgumentException {
            if (n < 5 || n > 52) {
                throw new IllegalArgumentException("Please pick an integer between 5 and 52.");
            }
            else {
                ArrayList<PlayingCard> handOfCards = new ArrayList<>();
                Random random = new Random();
                for (int i = 0; i < n; i++) {
                    int numberOfCards = getPlayingCards().size();
                    int j = random.nextInt(numberOfCards);
                    PlayingCard newPlayingCard = getPlayingCards().get(j);
                    handOfCards.add(newPlayingCard);
                    getPlayingCards().remove(newPlayingCard);
                }
                playingCards = handOfCards;
            }
        }

        public ArrayList<PlayingCard> getPlayingCardsHand() {
            return playingCards;
        }
    }

    public ArrayList<PlayingCard> dealHand(int n) {
        HandOfCards handOfCards = new HandOfCards(n);
        return handOfCards.getPlayingCardsHand();
    }

    @Override
    public String toString() {
        String allPlayingCards = "";
        int i = 0;
        for(PlayingCard playingCard : playingCards) {
            String playingCardString = playingCard.getAsString();
            allPlayingCards += playingCardString + " ";
            i++;
            if(i > 12) {
                allPlayingCards += "\n";
                i = 0;
            }
        }
        return allPlayingCards;
    }
}
