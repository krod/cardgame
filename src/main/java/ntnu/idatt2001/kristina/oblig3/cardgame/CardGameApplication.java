package ntnu.idatt2001.kristina.oblig3.cardgame;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CardGameApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    ArrayList<PlayingCard> currentHandOfCards = new ArrayList<>();

    @Override
    public void start(Stage stage) throws Exception {
        Color cardGameGreen = Color.color(0.09019607843137254901960784313725,
                0.34509803921568627450980392156863,
                0.27843137254901960784313725490196);
        Color cardGameGrey = Color.color(224.0/255,223.0/255,220.0/255);

        Group startRoot = new Group();
        Scene startScene = new Scene(startRoot,cardGameGreen);

        stage.setTitle("Card game");
        stage.setWidth(1200);
        stage.setHeight(680);

        // STAGE ICON
        Image stageIcon = new Image(new FileInputStream("C:\\IDATT2001\\Java\\Cardgame\\src\\cardGameIcon.png"));
        stage.getIcons().add(stageIcon);



        //START SCEENE
        // WELCOME TEXT
        Text welcomeText = new Text("Welcome to this card game!");
        welcomeText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 30));
        welcomeText.setFill(Color.DARKSEAGREEN);
        welcomeText.setX(450);
        welcomeText.setY(270);
        startRoot.getChildren().add(welcomeText);

        // UNDERLINE FOR WELCOME TEXT
        Line underline = new Line();
        underline.setStartX(500);
        underline.setStartY(285);
        underline.setEndX(760);
        underline.setEndY(285);
        underline.setStrokeWidth(2);
        underline.setStroke(Color.DARKSEAGREEN);
        startRoot.getChildren().add(underline);

        // MADE BY TEXT
        Text madeByText = new Text("Made by Kristina Ødegård");
        madeByText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 8));
        madeByText.setFill(Color.DARKSEAGREEN);
        madeByText.setX(10);
        madeByText.setY(15);
        startRoot.getChildren().add(madeByText);

        // WELCOME IMAGE
        Image cardsImage = new Image(new FileInputStream("C:\\IDATT2001\\Java\\Cardgame\\src\\cards.png"));
        ImageView imageView = new ImageView(cardsImage);
        imageView.setX(520);
        imageView.setY(300);
        imageView.setFitHeight(180);
        imageView.setPreserveRatio(true);
        startRoot.getChildren().add(imageView);

        // PLAY BUTTON
        Button playButton = new Button("Start game");
        playButton.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 16));
        playButton.setLayoutX(585);
        playButton.setLayoutY(500);

        // NEW SCENE INITIATION
        Group playRoot = new Group();
        Scene playScene = new Scene(playRoot,cardGameGreen);

        playButton.setOnAction(e -> stage.setScene(playScene));
        startRoot.getChildren().add(playButton);



        // PLAY SCEENE
        // CARD VIEW BOX
        Rectangle cardViewBox = new Rectangle();
        cardViewBox.setFill(cardGameGrey);
        cardViewBox.setHeight(450);
        cardViewBox.setWidth(850);
        cardViewBox.setX(50);
        cardViewBox.setY(50);
        cardViewBox.setStrokeWidth(1);
        cardViewBox.setStroke(Color.BLACK);

        // SHADOW FOR CARD VIEW BOX
        DropShadow dropShadow = new DropShadow();
        dropShadow.setWidth(20);
        dropShadow.setHeight(20);
        dropShadow.setOffsetX(10);
        dropShadow.setOffsetY(10);
        dropShadow.setRadius(50);
        cardViewBox.setEffect(dropShadow);

        playRoot.getChildren().add(cardViewBox);

        // CARD CONTAINER & ANALYSIS CONTAINER
        Group cardContainer = new Group();
        Group analysisContainer = new Group();
        playRoot.getChildren().addAll(cardContainer, analysisContainer);

        // SUM OF THE FACES TEXT
        Text sumOfTheFacesText = new Text("Sum of the faces: ");
        sumOfTheFacesText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
        sumOfTheFacesText.setFill(Color.DARKSEAGREEN);
        sumOfTheFacesText.setX(925);
        sumOfTheFacesText.setY(270);

        // CARDS OF HEARTS TEXT
        Text cardsOfHeartsText = new Text("Cards of hearts: ");
        cardsOfHeartsText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
        cardsOfHeartsText.setFill(Color.DARKSEAGREEN);
        cardsOfHeartsText.setX(935);
        cardsOfHeartsText.setY(330);

        // QUEEN OF SPADES TEXT
        Text queenOfSpadesText = new Text("Queen of Spades: ");
        queenOfSpadesText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
        queenOfSpadesText.setFill(Color.DARKSEAGREEN);
        queenOfSpadesText.setX(925);
        queenOfSpadesText.setY(310);

        // FLUSH MESSAGE
        Text flushText = new Text("Flush: ");
        flushText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
        flushText.setFill(Color.DARKSEAGREEN);
        flushText.setX(998);
        flushText.setY(290);

        playRoot.getChildren().addAll(sumOfTheFacesText, cardsOfHeartsText, queenOfSpadesText, flushText);

        // DEAL HAND BUTTON
        Button dealHandButton = new Button("Deal hand");
        dealHandButton.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 18));
        dealHandButton.setLayoutX(500);
        dealHandButton.setLayoutY(565);

        // NUMBER OF CARDS
        TextField numberOfCards = new TextField();
        numberOfCards.setLayoutX(450);
        numberOfCards.setLayoutY(570);
        numberOfCards.setMaxWidth(30);

        Text numberOfCardsText = new Text("Number of cards:");
        numberOfCardsText.setLayoutX(330);
        numberOfCardsText.setLayoutY(585);
        numberOfCardsText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
        numberOfCardsText.setFill(Color.DARKSEAGREEN);
        playRoot.getChildren().add(numberOfCardsText);

        dealHandButton.setOnAction(event -> {
            currentHandOfCards.clear();
            cardContainer.getChildren().clear();
            analysisContainer.getChildren().clear();
            try {
                DeckOfCards deck = new DeckOfCards();
                int n = Integer.parseInt(numberOfCards.getText());
                ArrayList<PlayingCard> handOfCards = deck.dealHand(n);
                currentHandOfCards.addAll(handOfCards);
                int X = 100;
                int Y = 100;
                for(int i = 0; i < handOfCards.size(); i++) {
                    if (i != 0 && i % 8 == 0) {
                        X = 100;
                        Y += 100;
                    }
                    Text cardText = new Text(handOfCards.get(i).getAsString());
                    cardText.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 35));
                    cardText.setX(X);
                    cardText.setY(Y);
                    cardContainer.getChildren().add(cardText);
                    X += 100;
                }
            }
            catch (IllegalArgumentException exception) {
                AlertBox.display("Illegal input","Please enter a number between 5 and 52.");
                numberOfCards.clear();
            }
        });

        playRoot.getChildren().addAll(dealHandButton, numberOfCards);

        // CHECK HAND BUTTON
        Button checkHandButton = new Button("Check hand");
        checkHandButton.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 18));
        checkHandButton.setLayoutX(1000);
        checkHandButton.setLayoutY(70);

        checkHandButton.setOnAction(event -> {
            if(cardContainer.getChildren().size() < 1) {
                AlertBox.display("No cards", "Please deal cards first.");
            }
            else {
                analysisContainer.getChildren().clear();

                // SUM OF CARDS (ANALYSIS)
                int sumOfCardsInt = currentHandOfCards.stream().map(PlayingCard::getFace).reduce(Integer::sum).get();
                Text sumOfCards =  new Text(String.valueOf(sumOfCardsInt));
                sumOfCards.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
                sumOfCards.setFill(Color.DARKSEAGREEN);
                sumOfCards.setX(1040);
                sumOfCards.setY(270);


                // CARDS OF HEARTS (ANALYSIS)
                List<PlayingCard> cardsOfHearsList = currentHandOfCards.stream().filter(playingCard ->
                        (playingCard.getSuit()) == 'H').collect(Collectors.toList());
                Text cardsOfHearts = new Text("");
                if (cardsOfHearsList.size() < 1) {
                    cardsOfHearts.setText("None");
                }
                else {
                    cardsOfHearsList.forEach(card -> cardsOfHearts.setText(cardsOfHearts.getText() + card.getAsString() + "\n"));
                }

                cardsOfHearts.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
                cardsOfHearts.setFill(Color.DARKSEAGREEN);
                cardsOfHearts.setX(1040);
                cardsOfHearts.setY(330);

                // QUEEN OF SPADES (ANALYSIS)
                List<PlayingCard> queenOfSpadesList = currentHandOfCards.stream().filter(playingCard ->
                        playingCard.getSuit() == 'S' && playingCard.getFace() == 12).collect(Collectors.toList());
                Text queenOfSpades;
                if(queenOfSpadesList.size() == 1) {
                    queenOfSpades = new Text("Yes");
                }
                else {
                    queenOfSpades = new Text("No");
                }
                queenOfSpades.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
                queenOfSpades.setFill(Color.DARKSEAGREEN);
                queenOfSpades.setX(1040);
                queenOfSpades.setY(310);

                // FLUSH (ANALYSIS)
                List<PlayingCard> heartsList = currentHandOfCards.stream().filter(playingCard ->
                        playingCard.getSuit() == 'H').collect(Collectors.toList());
                List<PlayingCard> diamondsList = currentHandOfCards.stream().filter(playingCard ->
                        playingCard.getSuit() == 'D').collect(Collectors.toList());
                List<PlayingCard> clubsList = currentHandOfCards.stream().filter(playingCard ->
                        playingCard.getSuit() == 'C').collect(Collectors.toList());
                List<PlayingCard> spadesList = currentHandOfCards.stream().filter(playingCard ->
                        playingCard.getSuit() == 'S').collect(Collectors.toList());

                Text flush = new Text("");
                if (heartsList.size() >= 5 || diamondsList.size() >= 5 || clubsList.size() >= 5 || spadesList.size() >= 5) {
                    flush.setText("Yes");
                }
                else {
                    flush.setText("No");
                }

                flush.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 14));
                flush.setFill(Color.DARKSEAGREEN);
                flush.setX(1040);
                flush.setY(290);

                analysisContainer.getChildren().addAll(sumOfCards, cardsOfHearts, queenOfSpades, flush);
            }

        });

        playRoot.getChildren().add(checkHandButton);

        // CLEAR BUTTON
        Button clearButton = new Button("Clear table");
        clearButton.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 10));
        clearButton.setLayoutX(150);
        clearButton.setLayoutY(15);

        clearButton.setOnAction(f -> {
            currentHandOfCards.clear();
            cardContainer.getChildren().clear();
            analysisContainer.getChildren().clear();
            numberOfCards.clear();
        });
        playRoot.getChildren().add(clearButton);

        // EXIT BUTTON
        Button exitButton = new Button("Exit game");
        exitButton.setFont(Font.font("Trebuchet MS", FontWeight.EXTRA_LIGHT, 10));
        exitButton.setLayoutX(50);
        exitButton.setLayoutY(15);

        exitButton.setOnAction(f -> {
            currentHandOfCards.clear();
            cardContainer.getChildren().clear();
            analysisContainer.getChildren().clear();
            numberOfCards.clear();
            stage.setScene(startScene);
        });
        playRoot.getChildren().add(exitButton);

        stage.setScene(startScene);
        stage.show();
    }
}