package ntnu.idatt2001.kristina.oblig3.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    @Nested
    class DeckCreated {
        @Test
        @DisplayName("No equal cards in deck")
        public void deckSizeIs52() {
            DeckOfCards deck = new DeckOfCards();
            assertEquals(52, deck.getPlayingCards().size());
        }

        @Test
        @DisplayName("Deck size is 52")
        public void noEqualCardsInDeck() {
            DeckOfCards deck = new DeckOfCards();
            int equalCards = 0;
            for (PlayingCard thePlayingCard : deck.getPlayingCards()) {
                int temporaryEqualCards = -1;
                for (PlayingCard otherPlayingCard : deck.getPlayingCards()) {
                    if (thePlayingCard.equals(otherPlayingCard)) {
                        temporaryEqualCards ++;
                    }
                }
                equalCards += temporaryEqualCards;
            }
            assertEquals(0, equalCards);

        }
    }

    @Nested
    class DealHand {
        @Nested
        class ThrowsExceptions {
            @Test
            @DisplayName("Throws exception when too many cards")
            public void throwsExceptionWhenTooManyCards() throws IllegalArgumentException {
                DeckOfCards deck = new DeckOfCards();
                assertThrows(IllegalArgumentException.class,() -> deck.dealHand(53));
            }

            @Test
            @DisplayName("Throws exception when too few cards")
            public void throwsExceptionWhenNegativeNumberOfCards() throws IllegalArgumentException {
                DeckOfCards deck = new DeckOfCards();
                assertThrows(IllegalArgumentException.class,() -> deck.dealHand(3));
            }
        }
    }
}
